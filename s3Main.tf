resource "aws_s3_bucket" "TestBucket" {
  bucket = "${var.bucket_name}"
  acl    = "private"

  tags = {
    Name = "Managed_Currax"
  }

    versioning {
    enabled = true
  }
}
